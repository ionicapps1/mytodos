export interface IToDo {
    title: string;
    checked: boolean;
    key?;
}
