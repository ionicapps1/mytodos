import {Component} from '@angular/core';
import {IToDo} from '../interface/ito-do';
import {AngularFireDatabase} from '@angular/fire/database';
import {TodoProviderService} from '../provider/todo-provider.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss']
})
export class HomePage {

    myTodos: IToDo[];
    adding: boolean;
    todoTitle: string;

    constructor(public fbdb: AngularFireDatabase, public todoProviderService: TodoProviderService) {
    }

    addTodo() {
        const todo: IToDo = {
            title: this.todoTitle,
            checked: false
        };
        this.myTodos.push(todo);
        this.adding = false;
        this.todoTitle = '';
        this.saveTodo(todo);
        this.fillTodos();
    }

    changeState(todo) {
        this.updateTodo(todo);
    }

    ionViewWillEnter() {
        this.adding = false;
        this.fillTodos();
    }

    fillTodos() {
        this.todoProviderService.getTodos().subscribe(data => {
            this.myTodos = [];
            data.forEach(action => {
                this.myTodos.push({
                    key: action.key,
                    title: action.payload.exportVal().title,
                    checked: action.payload.exportVal().checked
                });
                console.log(this.myTodos);
            });
        });
    }

    removeTodo(todo: IToDo) {
        const index: number = this.myTodos.indexOf(todo);
        if (index !== -1) {
            console.log('removing object');
            this.myTodos.splice(index, 1);
            this.todoProviderService.removeToDo(todo);
        }
    }

    updateTodo(todo) {
        this.todoProviderService.updateTodo(todo);
    }

    saveTodo(todo) {
        this.todoProviderService.saveToDo(todo);
    }

    showAddToDo() {
        this.adding = !this.adding;
    }

}
