import {Injectable} from '@angular/core';
import {IToDo} from '../interface/ito-do';

import {Storage} from '@ionic/storage';
import {AngularFireDatabase} from '@angular/fire/database';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TodoProviderService {

    constructor(private afdb: AngularFireDatabase) {
        console.log('Hello FavoriteMovieProvider Provider');
    }

    saveToDo(todo: IToDo) {
        this.afdb.list('Todo/').push(todo);
    }

    updateTodo(todo: IToDo) {
        this.afdb.object('Todo/' + todo.key + '/checked/').set(todo.checked);
    }

    removeToDo(todo: IToDo) {
        this.afdb.list('Todo/').remove(todo.key);
    }

    getTodos(): Observable<any> {
        return this.afdb.list('Todo/').snapshotChanges(['child_added', 'child_removed']);
    }

}
