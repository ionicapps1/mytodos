import { TestBed } from '@angular/core/testing';

import { TodoProviderService } from './todo-provider.service';

describe('TodoProviderService', () => {
  let service: TodoProviderService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TodoProviderService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
